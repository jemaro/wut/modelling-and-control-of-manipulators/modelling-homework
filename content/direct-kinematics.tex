\section{Definitions}
For the sake of shorter expressions the abbreviations described in
\autoref{eqn:abbreviations} are taken into account for the rest of the
report.

\begin{center}\begin{equation}
\begin{array}{c}
 cos(\theta_i)\to c_i \\
 sin(\theta_i)\to s_i \\
 cos(\theta_i+\theta_j)\to c_{i+j} \\
 sin(\theta_i+\theta_j)\to s_{i+j} \\
\end{array}\label{eqn:abbreviations}\end{equation}\end{center}

Additionally, one will decompose the homography $\mathcal{T}$ into its
subcomponents using the notation found in \autoref{eqn:Td}.

\begin{doublespace}
\begin{center}\begin{equation}\text{$\mathcal{T}$}=\left(
\begin{array}{cccc}
 r_{11} & r_{12} & r_{13} & p_x \\
 r_{21} & r_{22} & r_{23} & p_y \\
 r_{31} & r_{32} & r_{33} & p_z \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)\label{eqn:Td}\end{equation}\end{center}
\end{doublespace}

\section{Denavit-Hartenberg parameters for IRb-7600 manipulator}

One can model the IRb-7600 manipulator as shown in the \autoref{fig:diagram},
obtaining the Denavit-Hatenberg parameters shown in \autoref{eqn:dh}. For the
sake of reusability of the analytical solution that has been derived, $a_i$ and
$d_i$ parameters with a constant non-zero numerical value has been treated as
constants without specifying the numerical value. 

\begin{figure}[h]
    \includegraphics[width=\textwidth]{\subdir/Diagram.jpg}
    \caption{Irb-7600 manipulator diagram following the modified Denavit-Hatenberg convention}
    \label{fig:diagram}
\end{figure}

\begin{doublespace}
\begin{center}\begin{equation}\begin{array}{llllllllllllllllll}
    \text{a0}\to 0 & \text{$\alpha$0}\to 0 & \text{d1}\to 0 \\
    \text{a1}\to 410 & \text{$\alpha$1}\to \frac{3 \pi }{2} & \text{d2}\to 0 \\
    \text{a2}\to 1075 & \text{$\alpha$2}\to 0 & \text{d3}\to 0 \\
    \text{a3}\to 165 & \text{$\alpha$3}\to \frac{3 \pi }{2} & \text{d4}\to 1056 & \\
    \text{a4}\to 0 & \text{$\alpha $4}\to \frac{\pi }{2} & \text{d5}\to 0 \\
    \text{a5}\to 0 & \text{$\alpha $5}\to \frac{3 \pi }{2} & \text{d6}\to 250 \\
\end{array}\label{eqn:dh}\end{equation}\end{center}
\end{doublespace}

\section{Direct Kinematics Problem}
Knowing the values of joint variables $\theta_i$ compute the homogenous matrix
${}^0_6\mathcal{T}$ that corresponds to the frame attached to the end-effector.

Using \autoref{eqn:T}, the intermediate homographies are calculated
progressively until ${}^0_6\mathcal{T}$, which can be found in
\autoref{eqn:T06}. As the matrix corresponding to ${}^0_6\mathcal{T}$ would not
fit properly in a printed format, the homography has been represented using its
components previously defined in \autoref{eqn:Td}.

\begin{equation}
    {}^0_n\mathcal{T}=\prod^n_ {i=1}{}^{i-1}_i\mathcal{T}
    \label{eqn:T}
\end{equation}

\begin{center}\begin{equation}\text{${}^0_1\mathcal{T}$=}\left(
\begin{array}{cccc}
 c_1 & -s_1 & 0 & 0 \\
 s_1 & c_1 & 0 & 0 \\
 0 & 0 & 1 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eq:T01}\end{equation}\end{center}

\begin{center}\begin{equation}\text{${}^0_2\mathcal{T}$=}\left(
\begin{array}{cccc}
 c_1 c_2 & -c_1 s_2 & -s_1 & a_1 c_1 \\
 c_2 s_1 & -s_1 s_2 & c_1 & a_1 s_1 \\
 -s_2 & -c_2 & 0 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)\end{equation}\end{center}

\begin{center}\begin{equation}\text{${}^0_3\mathcal{T}$=}\left(
\begin{array}{cccc}
 c_1 c_{\text{2+3}} & -c_1 s_{\text{2+3}} & -s_1 & c_1 \left(a_1+a_2 c_2\right) \\
 c_{\text{2+3}} s_1 & -s_1 s_{\text{2+3}} & c_1 & \left(a_1+a_2 c_2\right) s_1 \\
 -s_{\text{2+3}} & -c_{\text{2+3}} & 0 & -a_2 s_2 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)\end{equation}\end{center}

\begin{center}\begin{equation}\text{${}^0_4\mathcal{T}$=}\left(
\begin{array}{cccc}
 c_1 c_4 c_{\text{2+3}}+s_1 s_4 & c_4 s_1-c_1 c_{\text{2+3}} s_4 & -c_1 s_{\text{2+3}} & c_1 \left(a_1+a_2 c_2+a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}\right)
\\
 c_2 c_3 c_4 s_1-c_4 s_1 s_2 s_3-c_1 s_4 & -c_1 c_4-c_{\text{2+3}} s_1 s_4 & -s_1 s_{\text{2+3}} & s_1 \left(a_1+a_2 c_2+a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}\right)
\\
 -c_4 s_{\text{2+3}} & s_4 s_{\text{2+3}} & -c_{\text{2+3}} & -c_{\text{2+3}} d_4-a_2 s_2-a_3 s_{\text{2+3}} \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)\end{equation}\end{center}

\begin{center}\begin{equation}\text{${}^0_5\mathcal{T}$ $\to $}
\begin{array}{l}
 r_{11}\to c_5 s_1 s_4+c_1 \left(c_4 c_5 c_{\text{2+3}}-s_5 s_{\text{2+3}}\right) \\
 r_{12}\to -s_1 s_4 s_5-c_1 \left(c_4 c_{\text{2+3}} s_5+c_5 s_{\text{2+3}}\right) \\
 r_{13}\to -c_4 s_1+c_1 c_{\text{2+3}} s_4 \\
 p_x\to c_1 \left(a_1+a_2 c_2+a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}\right) \\
 r_{21}\to c_5 \left(c_2 c_3 c_4 s_1-c_4 s_1 s_2 s_3-c_1 s_4\right)-s_1 s_5 s_{\text{2+3}} \\
 r_{22}\to -c_2 c_5 s_1 s_3+\left(c_4 s_1 s_2 s_3+c_1 s_4\right) s_5-c_3 s_1 \left(c_5 s_2+c_2 c_4 s_5\right) \\
 r_{23}\to c_1 c_4+c_{\text{2+3}} s_1 s_4 \\
 p_y\to s_1 \left(a_1+a_2 c_2+a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}\right) \\
 r_{31}\to -c_{\text{2+3}} s_5-c_4 c_5 s_{\text{2+3}} \\
 r_{32}\to s_2 \left(c_5 s_3+c_3 c_4 s_5\right)+c_2 \left(-c_3 c_5+c_4 s_3 s_5\right) \\
 r_{33}\to -s_4 s_{\text{2+3}} \\
 p_z\to -c_{\text{2+3}} d_4-a_2 s_2-a_3 s_{\text{2+3}} \\
\end{array}\end{equation}\end{center}

\begin{center}\begin{equation}\text{${}^0_6\mathcal{T}$ $\to $}
\begin{array}{l}
 r_{11}\to s_1 \left(c_5 c_6 s_4+c_4 s_6\right)+c_1 \left(c_{\text{2+3}} \left(c_4 c_5 c_6-s_4 s_6\right)-c_6 s_5 s_{\text{2+3}}\right) \\
 r_{12}\to c_6 \left(c_4 s_1-c_1 c_{\text{2+3}} s_4\right)-s_6 \left(c_5 s_1 s_4+c_1 \left(c_4 c_5 c_{\text{2+3}}-s_5 s_{\text{2+3}}\right)\right)
\\
 r_{13}\to -s_1 s_4 s_5-c_1 \left(c_4 c_{\text{2+3}} s_5+c_5 s_{\text{2+3}}\right) \\
 p_x\to -d_6 s_1 s_4 s_5+c_1 \left(a_1+a_2 c_2+c_{\text{2+3}} \left(a_3-c_4 d_6 s_5\right)-d_4 s_{\text{2+3}}-c_5 d_6 s_{\text{2+3}}\right) \\
 r_{21}\to -\left(c_1 c_4+c_{\text{2+3}} s_1 s_4\right) s_6+c_6 \left(c_5 \left(c_2 c_3 c_4 s_1-c_4 s_1 s_2 s_3-c_1 s_4\right)-s_1 s_5 s_{\text{2+3}}\right)
\\
 r_{22}\to c_1 \left(-c_4 c_6+c_5 s_4 s_6\right)+s_1 \left(-c_6 c_{\text{2+3}} s_4+s_6 \left(-c_2 c_3 c_4 c_5+c_4 c_5 s_2 s_3+s_5 s_{\text{2+3}}\right)\right)
\\
 r_{23}\to -c_2 c_5 s_1 s_3+\left(c_4 s_1 s_2 s_3+c_1 s_4\right) s_5-c_3 s_1 \left(c_5 s_2+c_2 c_4 s_5\right) \\
 p_y\to c_1 d_6 s_4 s_5+s_1 \left(a_1-c_3 \left(d_4+c_5 d_6\right) s_2-a_3 s_2 s_3+c_4 d_6 s_2 s_3 s_5+c_2 \left(a_2-\left(d_4+c_5 d_6\right) s_3+c_3
\left(a_3-c_4 d_6 s_5\right)\right)\right) \\
 r_{31}\to -c_6 \left(c_3 \left(c_4 c_5 s_2+c_2 s_5\right)+s_3 \left(c_2 c_4 c_5-s_2 s_5\right)\right)+s_4 s_6 s_{\text{2+3}} \\
 r_{32}\to c_3 \left(c_6 s_2 s_4+\left(c_4 c_5 s_2+c_2 s_5\right) s_6\right)+s_3 \left(-s_2 s_5 s_6+c_2 \left(c_6 s_4+c_4 c_5 s_6\right)\right) \\
 r_{33}\to s_2 \left(c_5 s_3+c_3 c_4 s_5\right)+c_2 \left(-c_3 c_5+c_4 s_3 s_5\right) \\
 p_z\to -c_2 \left(c_3 \left(d_4+c_5 d_6\right)+s_3 \left(a_3-c_4 d_6 s_5\right)\right)+s_2 \left(-a_2+\left(d_4+c_5 d_6\right) s_3+c_3 \left(-a_3+c_4
d_6 s_5\right)\right) \\
\end{array}\label{eqn:T06}\end{equation}\end{center}

These solutions have been validated using the initial joint variables that can
be extracted from \autoref{fig:diagram}, knowing the corresponding end-effector
frame position and orientation. Refer to the Mathematica notebook for more information.
